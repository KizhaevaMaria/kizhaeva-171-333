//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tilist;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormShow(TObject *Sender)
{
	dm->FDConnection1->Connected = true;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddClick(TObject *Sender)
{
	dm->taNotes->Append();
    tc->GotoVisibleTab(tiitem->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->taNotes->Edit();
	tc->GotoVisibleTab(tiitem->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSaveClick(TObject *Sender)
{
	dm->taNotes->Post();
	tc->GotoVisibleTab(tilist->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCancelClick(TObject *Sender)
{
	dm->taNotes->Cancel();
	tc->GotoVisibleTab(tilist->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDeleteClick(TObject *Sender)
{
	dm->taNotes->Delete();
	tc->GotoVisibleTab(tilist->Index);
}
//---------------------------------------------------------------------------


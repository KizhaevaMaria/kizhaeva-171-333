object dm: Tdm
  OldCreateOrder = False
  Height = 298
  Width = 402
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Users\'#1052#1072#1088#1080#1103' '#1050#1080#1078#1072#1077#1074#1072'\Downloads\Notes.db'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    BeforeConnect = FDConnection1BeforeConnect
    Left = 72
    Top = 40
  end
  object taNotes: TFDTable
    Connection = FDConnection1
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 112
    Top = 128
    object taNotesCaption: TStringField
      FieldName = 'Caption'
      Origin = 'Caption'
      Required = True
      Size = 50
    end
    object taNotesPriority: TSmallintField
      FieldName = 'Priority'
      Origin = 'Priority'
      Required = True
    end
    object taNotesDetail: TStringField
      FieldName = 'Detail'
      Origin = 'Detail'
      Size = 500
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 184
    Top = 136
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 256
    Top = 168
  end
end

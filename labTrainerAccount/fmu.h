//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buRestart;
	TButton *buAbout;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buYes;
	TButton *buNo;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TLabel *laCorrect;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *Label1;
	TLabel *laCode;
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	bool FAnswerCorrect;
	void DoReset();
	void DoContinue();
	void DoAnswer (bool aValue);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buStartClick(TObject *Sender) {
	randomCard();
	tc->Next();
	tb->Visible=true;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tc->First();
	tc->TabPosition = TTabPosition::None;
	tb->Visible=false;

	cardTexts = {
		L"��������� ������ ���������?",
		L"��������� ��������� ���������",
		L"���������� ���� �� ���������?",
		L"�������� �� �� ����?",
		L"���������� ����������?",
		L"�� ������� ��� ������?",
		L"��������������� ������?",
		L"��������� ���������� �� ����?",
		L"�������� ������ �� ����?",
		L"����� �� ��������� � ���������� ����������?",
		L"����� �� �������������� ������� �� ���������������� � �������?",
		L"���������� ������� � ���������?",
		L"����� � ���� ������� � �����?",
		L"����� � ���� � ��������?",
		L"���������� ������ ����� ����?",
		L"����������� � ���� �������� ��������?",
		L"������� � ����� �� ��������?",
		L"�������� ���� �������� ����� � �����?",
		L"�������� � ����������� ������������ �����?",
		L"�������� ���� � ���������?",
		L"�������� ������?",
		L"����� ������� � ����� ������� � �������?"};

	cardDif = {
		{-10, 10, 0, 20, -30, 0},
		{-50, 100, 0, 20, -10, 0},
		{-10, 0, 10, 0, 0, -10},
		{-30, 20, 0, 20, -20, 0},
		{-10, 10, 0, 10, 10, 0},
		{10, 0, -10, 40, -20, 10},
		{1, 0, -20, 0, 0, 10},
		{-20, 0, 20, 30, 0, 0},
		{-10, 20, 0, 10, -10, 0},
		{-10, -10, 30, 10, 0, 20},
		{30, 0, 10, 0, 30, 10},
		{-10, 0, 30, 20, 0, -10},
		{-10, -10, 10, 10, 20, 0},
		{0, 0, 20, 10, 10, -10},
		{0, -20, 30, 0, 20, 0},
		{0, 0, 10, 10, 0, -10},
		{-10, 0, 30, 10, 0, -10},
		{-20, -20, 20, 10, 20, 0},
		{-10, -10, 10, 0, 20, 0},
		{-20, -10, 20, 0, 0, -20},
		{0,0, 20, 0,0, -10},
		{0, -10, 20, 0, 0, -10}
		};

	cardChoices = {
		{L"������ �� �����", L"������� ������ ��������"},
		{L"��������-�� � ��������� � ��������", L"���� ��������"},
		{L"�� ����� ������ � ������", L"������ ������� �� ������ �������"},
		{L"������� �� �������", L"� ��������� � ���� ������"},
		{L"Yes", L"No"},
		{L"����", L"���"},
		{L"��", L"��������"},
		{L"����� ��� � ����������� ���� ����������, ���", L"���"},
		{L"zzZ", L"� �������"},
		{L"��", L"��!"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��!", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"},
		{L"��", L"���"}};
}

// --------------------------------------------------------------------------
void TForm1::randomCard() {
	cardIndex = Random(cardTexts.size());
	cardImg->ImageIndex = cardIndex;
	cardText->Text = cardTexts[cardIndex];
	buLeft->Text = cardChoices[cardIndex][0];
	buRight->Text = cardChoices[cardIndex][1];
}

void __fastcall TForm1::buRightClick(TObject *Sender) {
	changeStat(cardDif[cardIndex][3],cardDif[cardIndex][4],cardDif[cardIndex][5]);
}

void __fastcall TForm1::buLeftClick(TObject *Sender) {
	changeStat(cardDif[cardIndex][0],cardDif[cardIndex][1],cardDif[cardIndex][2]);
}
// ---------------------------------------------------------------------------
void TForm1::changeStat(int study, int sleep, int entertainment) {
	flAniStudy->Enabled=false;
	flAniSleep->Enabled=false;
	flAniEnter->Enabled=false;

	lbCountStudy->Text = StrToInt(lbCountStudy->Text) + study;
	lbCountSleep->Text = StrToInt(lbCountSleep->Text) + sleep;
	lbCountEntertainment->Text = StrToInt(lbCountEntertainment->Text) + entertainment;

	if(study!=0){
	flAniStudy->Enabled=true;
	}
	if(sleep!=0){
	flAniSleep->Enabled=true;
	}
	if(entertainment!=0){
	flAniEnter->Enabled=true;
	}

	changeRang(study, sleep, entertainment);

	if (StrToInt(lbCountStudy->Text) >= 100) {
		resultFormer(L"�� ����� � ��� �� ������ ���������� ������.");
		tc->Next();
		return;
	}
	if (StrToInt(lbCountStudy->Text) <= 0) {
		resultFormer(L"��� ��������� �� ������ ������������.");
		tc->Next();
		return;
	}
	if (StrToInt(lbCountSleep->Text) >= 100) {
		resultFormer(L"�� ������� ��������.");
		tc->Next();
		return;
	}
	if (StrToInt(lbCountSleep->Text) <= 0) {
		resultFormer(L"�� ���������� ��� �� �������� ���� ����.");
		tc->Next();
		return;
	}
	if (StrToInt(lbCountEntertainment->Text) >= 100) {
		resultFormer(L"�� �������� � ����� ��������.");
		tc->Next();
		return;
	}
	if (StrToInt(lbCountEntertainment->Text) <= 0) {
		resultFormer(L"�� ���������� ����������� � ����� �� ��������� � �����.");
		tc->Next();
		return;
	}
	randomCard();
	lbCountDays->Text = StrToInt(lbCountDays->Text) + 1;

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buRestartClick(TObject *Sender)
{
	tb->Visible=false;
	(lbCountStudy->Text) = L"50";
	(lbCountSleep->Text) = L"50";
	(lbCountEntertainment->Text) = L"50";
	(lbCountDays->Text) = L"0";
	buStart->Enabled=false;
	edName->Text=L"";
	studyRang=0;
	sleepRang=0;
	enterRang=0;
	tc->First();
}
//---------------------------------------------------------------------------
void TForm1::resultFormer(UnicodeString result){
	lbResult->Text = result;
	memo -> Lines-> Add (rang + " " + edName -> Text + L" ���������� ����: " + lbCountDays -> Text);
}
void __fastcall TForm1::edNameChange(TObject *Sender)
{
	name = edName->Text;
	if(name!=L""){
	buStart -> Enabled = true ;
	}
}
//---------------------------------------------------------------------------
void TForm1::changeRang(int study, int sleep, int entertainment){

	if(study>0){
	studyRang++;
	}

	if(sleep>0){
	sleepRang++;
	}

	if(entertainment>0){
	enterRang++;
	}

	if(studyRang == 2){
	ShowMessage(L"��������� �����. �������� ������ - ��������");
	rang = L"��������";
	studyRang++;
	}

	if(sleepRang == 2){
	ShowMessage(L"������� ����� �����. �������� ������ - ����");
	rang = L"����";
	sleepRang++;
	}

	if(enterRang == 2){
	ShowMessage(L"��������� �� ����� �� �������. �������� ������ - ��������");
	rang = L"��������";
	enterRang++;
	}
}


void __fastcall TForm1::Button1Click(TObject *Sender)
{
	ShowMessage(L"� ���� ���� ��� ���������� ������� ������������ ���������� ����. ��� � � ������� ����� �������� ����������� ���������� � �������� ��, ��� ��� ������������� ����������. ���� ������� ������� �������� - ���� ��� �������. ���� ����������, ����� ���������� ��������� ������� 0 ���� 100. �����!");
}
//---------------------------------------------------------------------------


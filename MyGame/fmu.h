//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <System.ImageList.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Objects.hpp>
#include <vector>
using namespace std;
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TTabControl *tc;
	TTabItem *Menu;
	TTabItem *Game;
	TTabItem *Result;
	TButton *buStart;
	TImageList *ImageList1;
	TGlyph *cardImg;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buLeft;
	TButton *buRight;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *lbStudy;
	TLabel *lbSleep;
	TLabel *lbEntertainment;
	TLabel *lbDays;
	TLabel *lbCountStudy;
	TLabel *lbCountSleep;
	TLabel *lbCountEntertainment;
	TLabel *lbCountDays;
	TEdit *edName;
	TLabel *Name;
	TGridPanelLayout *GridPanelLayout3;
	TMemo *memo;
	TLabel *lbResult;
	TButton *buRestart;
	TFloatAnimation *flAniStudy;
	TFloatAnimation *flAniSleep;
	TFloatAnimation *flAniEnter;
	TLabel *cardText;
	TRectangle *Rectangle1;
	TButton *Button1;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buRightClick(TObject *Sender);
	void __fastcall buLeftClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall edNameChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
	void randomCard();
	int cardIndex;
	vector<UnicodeString> cardTexts;
	vector<vector<int>> cardDif;
	vector<vector<UnicodeString>> cardChoices;
	void changeStat(int study, int sleep, int entertainment);
	void resultFormer (UnicodeString result);
	UnicodeString name;
	void changeRang(int study, int sleep, int entertainment);
	int studyRang;
	int sleepRang;
	int enterRang;
	UnicodeString rang;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

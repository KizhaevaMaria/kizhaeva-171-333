//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TButton *buBack;
	TButton *buAbout;
	TLabel *Label1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiTime;
	TTabItem *tiDistance;
	TTabItem *tiQi;
	TTabItem *tiTemperature;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buTime;
	TButton *buDistance;
	TButton *buQi;
	TButton *buTemperatura;
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TEdit *edMs;
	TEdit *edSec;
	TEdit *edMinutes;
	TEdit *edHours;
	TEdit *edDays;
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem6;
	TEdit *edMm;
	TListBoxItem *ListBoxItem7;
	TEdit *edCm;
	TListBoxItem *ListBoxItem8;
	TEdit *edDm;
	TListBoxItem *ListBoxItem9;
	TEdit *edM;
	TListBoxItem *ListBoxItem10;
	TEdit *edKm;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buTimeClick(TObject *Sender);
	void __fastcall buDistanceClick(TObject *Sender);
	void __fastcall buQiClick(TObject *Sender);
	void __fastcall buTemperaturaClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall Label1Click(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall edDistanceKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->First();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buTimeClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiTime->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buDistanceClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiDistance->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buQiClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiQi->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buTemperaturaClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiTemperature->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::tcChange(TObject *Sender)
{
	buBack->Visible = (tc->ActiveTab != tiMenu);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift)
{
	long double x;
	long double xSec;
	x = StrToFloatDef (((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag) {
	case 1: xSec = x / 1000; break;
	case 2: xSec = x; break;
	case 3:xSec = x*60; break;
	case 4:xSec = x*60*60; break;
	case 5:xSec = x*60*60*24; break;
	}
	edMs->Text = FloatToStr(xSec*1000);
	edSec->Text = FloatToStr(xSec);
	edMinutes->Text = FloatToStr(xSec/60);
	edHours->Text = FloatToStr(xSec/60/60);
	edDays->Text = FloatToStr(xSec/60/60/24);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TForm1::buBackClick(TObject *Sender)
{
				tc->First();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edDistanceKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift)
{
		long double x;
	long double xM;
	x = StrToFloatDef (((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag) {
	case 1: xM = x / 1000; break;
	case 2: xM = x / 100; break;
	case 3:xM = x/10; break;
	case 4:xM = x; break;
	case 5:xM = x*1000; break;
	}
	edMn->Text = FloatToStr(x / 1000);
	edCm->Text = FloatToStr(x / 100);
	edDm->Text = FloatToStr(x/10);
	edM->Text = FloatToStr(x);
	edKm->Text = FloatToStr(x*1000);
}
//---------------------------------------------------------------------------

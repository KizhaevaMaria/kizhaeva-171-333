//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	FCounterPeach = 0;
	FCounterSpiderMan = 0;

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image1Click(TObject *Sender)
{
	FCounterPeach++;
   LaCounterPeach ->Text = "������ = " + IntToStr(FCounterPeach);
   FloatAnimation3->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image2Click(TObject *Sender)
{
	FCounterSpiderMan++;
   LaCounterSpiderMan ->Text = "������� ���� = " + IntToStr(FCounterSpiderMan);
   FloatAnimation4->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormResize(TObject *Sender)
{
	FloatAnimation1->StopValue = this->Width - Image1->Width;
	FloatAnimation2->StopValue = this->Width - Image2->Width;
}
//---------------------------------------------------------------------------

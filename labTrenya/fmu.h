//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TMenu;
	TTabItem *TTime;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TTimer *tm;
	TLabel *laTimer;
	TTabItem *TRandom;
	TToolBar *ToolBar1;
	TButton *Button5;
	TLabel *Random;
	TLabel *laNum;
	TEdit *edNum;
	TButton *buCheck;
	TTabItem *TImage;
	TGridPanelLayout *GridPanelLayout2;
	TGlyph *Glyph1;
	TGlyph *Glyph2;
	TGlyph *Glyph3;
	TGlyph *Glyph4;
	TImageList *ImageList1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall buCheckClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
	TDateTime FTimeStart;
    TGlyph *FListGlyph[4];
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "fr.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::TMenuBlinchikiClick(TObject *Sender)
{
    tc->GotoVisibleTab (TBlinchiki->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab (TMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackMenuClick(TObject *Sender)
{
	tc->GotoVisibleTab (TMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab (TBlin->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackBlincikiClick(TObject *Sender)
{
	tc->GotoVisibleTab (TBlinchiki->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBlinVKrozinuClick(TObject *Sender)
{
	if( StrToInt(laNum->Text)> 0 ){

   
	TFrame1 * x = new TFrame1(glBucket);
	x->Parent = glBucket;
	x->Align = TAlignLayout::Client;
	x->Name = "fr" + IntToStr(dm->quProductID->Value);
	x->lafrName->Text = dm->quProductNAME->Value;
	x->frImage->Bitmap->Assign(dm->quProductIMAGE);
	x->lafrPrice->Text = dm->quProductPRICE->Value;
	x->lafrCount->Text = laNum->Text;

	glBucket->RecalcSize();

	tc->GotoVisibleTab(TVk->Index);

		tc->GotoVisibleTab (TVk->Index);
		}
	else {
		ShowMessage("�������� ���� �� ���� ��� ������ ������");
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMinusClick(TObject *Sender)
{
	if (StrToInt(laNum->Text) != 0) {
		laNum->Text = StrToInt(laNum->Text) - 1 ;
	}
	else {
		ShowMessage("������ ������");
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPlusClick(TObject *Sender)
{
	laNum->Text = StrToInt(laNum->Text) + 1 ;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackBlinClick(TObject *Sender)
{
	tc->GotoVisibleTab (TMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackVkClick(TObject *Sender)
{
	tc->GotoVisibleTab (TVk->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buVkClick(TObject *Sender)
{
	tc->GotoVisibleTab (TVk->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBistrZakazClick(TObject *Sender)
{
		tc->GotoVisibleTab (TOrder->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::addOtzivClick(TObject *Sender)
{
	tc->GotoVisibleTab (TAddOtzov->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackOtzivClick(TObject *Sender)
{
	tc->GotoVisibleTab (TOtzov->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAddOClick(TObject *Sender)
{
	if(edName->Text != "" || edPhone->Text != "" || edMail->Text != ""
	|| edMessage->Text !=""){
	dm->ReviewINS(edName->Text, edPhone->Text, edMail->Text, edMessage->Text);
	dm->quReview->Refresh();
	edName->Text = "";
	edPhone->Text = "";
	edMail->Text = "";
	edMessage->Text = "";
	ShowMessage("����� ��������");
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
	tc->GotoVisibleTab (TOtzov->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buOrderClick(TObject *Sender)
{
	dm->OrderingINS(edOrderName->Text, edOrderPhone->Text, edOrderMail->Text,
	edOrderAdress->Text, edOrderData->Text, edOrderTime->Text,
	edOrderPr->Text, edOrderPrice->Text);



}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button6Click(TObject *Sender)
{
	tc->GotoVisibleTab (TContackt->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button4Click(TObject *Sender)
{
	tc->GotoVisibleTab (TDelivery->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button8Click(TObject *Sender)
{
	tc->GotoVisibleTab (TOrder->Index);
    ShowMessage("���������� ������ � ������ ����������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
	ShowMessage("������� ���� ����������� �� ��������� :)");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	 ShowMessage("� ������ ����������");
}
//---------------------------------------------------------------------------


object dm: Tdm
  OldCreateOrder = False
  Height = 512
  Width = 565
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 80
    Top = 280
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 200
    Top = 280
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\'#1052#1072#1088#1080#1103' '#1050#1080#1078#1072#1077#1074#1072'\Documents\new folder with projec' +
        't\kizhaeva-171-333\Blinchiki\db\BLINCHIKI.fdb'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 80
    Top = 56
  end
  object quProduct: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.image,'
      '    product.gram,'
      '    product.kcal,'
      '    product.price,'
      '    product.note,'
      '    product.sticker'
      'from product'
      'order by product.category_id')
    Left = 200
    Top = 72
    object quProductID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProductCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quProductNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 90
    end
    object quProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
    object quProductGRAM: TIntegerField
      FieldName = 'GRAM'
      Origin = 'GRAM'
    end
    object quProductKCAL: TIntegerField
      FieldName = 'KCAL'
      Origin = 'KCAL'
    end
    object quProductPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object quProductNOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object quProductSTICKER: TIntegerField
      FieldName = 'STICKER'
      Origin = 'STICKER'
    end
  end
  object spOrdering_ListINS: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'ORDERING_LIST_INS'
    Left = 312
    Top = 144
    ParamData = <
      item
        Position = 1
        Name = 'ORDER_ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'PRODUCT_ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'PRODUCT_PRICE'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'QUANTITY'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object quOrders: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering.id,'
      '    ordering.create_datetime,'
      '    ordering.client_fio,'
      '    ordering.client_tel,'
      '    ordering.client_email,'
      '    ordering.client_address,'
      '    ordering.delivery_date,'
      '    ordering.delivery_time,'
      '    ordering.delivery_amount,'
      '    ordering.delivery_note,'
      '    ordering.status,'
      '    ordering.status_note'
      'from ordering'
      'order by ordering.id desc')
    Left = 288
    Top = 48
    object quOrdersID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrdersCREATE_DATETIME: TSQLTimeStampField
      FieldName = 'CREATE_DATETIME'
      Origin = 'CREATE_DATETIME'
      Required = True
    end
    object quOrdersCLIENT_FIO: TWideStringField
      FieldName = 'CLIENT_FIO'
      Origin = 'CLIENT_FIO'
      Size = 70
    end
    object quOrdersCLIENT_TEL: TWideStringField
      FieldName = 'CLIENT_TEL'
      Origin = 'CLIENT_TEL'
      Size = 15
    end
    object quOrdersCLIENT_EMAIL: TWideStringField
      FieldName = 'CLIENT_EMAIL'
      Origin = 'CLIENT_EMAIL'
      Size = 70
    end
    object quOrdersCLIENT_ADDRESS: TWideStringField
      FieldName = 'CLIENT_ADDRESS'
      Origin = 'CLIENT_ADDRESS'
      Size = 150
    end
    object quOrdersDELIVERY_DATE: TDateField
      FieldName = 'DELIVERY_DATE'
      Origin = 'DELIVERY_DATE'
    end
    object quOrdersDELIVERY_TIME: TTimeField
      FieldName = 'DELIVERY_TIME'
      Origin = 'DELIVERY_TIME'
    end
    object quOrdersDELIVERY_AMOUNT: TFloatField
      FieldName = 'DELIVERY_AMOUNT'
      Origin = 'DELIVERY_AMOUNT'
    end
    object quOrdersDELIVERY_NOTE: TWideStringField
      FieldName = 'DELIVERY_NOTE'
      Origin = 'DELIVERY_NOTE'
      Size = 4000
    end
    object quOrdersSTATUS: TIntegerField
      FieldName = 'STATUS'
      Origin = 'STATUS'
      Required = True
    end
    object quOrdersSTATUS_NOTE: TWideStringField
      FieldName = 'STATUS_NOTE'
      Origin = 'STATUS_NOTE'
      Size = 4000
    end
  end
  object quOrderingList: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering_list.id,'
      '    ordering_list.order_id,'
      '    ordering_list.product_id,'
      '    ordering_list.product_price,'
      '    ordering_list.quantity'
      'from ordering_list'
      'order by ordering_list.id ')
    Left = 360
    Top = 56
    object quOrderingListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrderingListORDER_ID: TIntegerField
      FieldName = 'ORDER_ID'
      Origin = 'ORDER_ID'
      Required = True
    end
    object quOrderingListPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Origin = 'PRODUCT_ID'
      Required = True
    end
    object quOrderingListPRODUCT_PRICE: TFloatField
      FieldName = 'PRODUCT_PRICE'
      Origin = 'PRODUCT_PRICE'
      Required = True
    end
    object quOrderingListQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
      Required = True
    end
  end
  object quReview: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    review.name,'
      '    review.phone,'
      '    review.mail,'
      '    review."MESSAGE"'
      'from review'
      'order by review.name')
    Left = 200
    Top = 152
    object quReviewNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 100
    end
    object quReviewPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      FixedChar = True
      Size = 11
    end
    object quReviewMAIL: TWideStringField
      FieldName = 'MAIL'
      Origin = 'MAIL'
      FixedChar = True
      Size = 100
    end
    object quReviewMESSAGE: TWideStringField
      FieldName = 'MESSAGE'
      Origin = '"MESSAGE"'
      FixedChar = True
      Size = 1000
    end
  end
  object spReviewINS: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'REVIEW_INS'
    Left = 408
    Top = 144
    ParamData = <
      item
        Position = 1
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 100
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 11
      end
      item
        Position = 3
        Name = 'MAIL'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 100
      end
      item
        Position = 4
        Name = 'MESS'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 1000
      end>
  end
  object spOrderINS: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'ORDERING_INS'
    Left = 344
    Top = 224
    ParamData = <
      item
        Position = 1
        Name = 'CLIENT_FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'CLIENT_TEL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'CLIENT_EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'CLIENT_ADDRESS'
        DataType = ftWideString
        ParamType = ptInput
        Size = 150
      end
      item
        Position = 5
        Name = 'DELIVERY_DATE'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 6
        Name = 'DELIVERY_TIME'
        DataType = ftTime
        ParamType = ptInput
      end
      item
        Position = 7
        Name = 'DELIVERY_AMOUNT'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Position = 8
        Name = 'DELIVERY_NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end>
  end
end

//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Edit.hpp>

//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TMenu;
	TTabItem *TBlinchiki;
	TToolBar *ToolBar1;
	TLabel *��������;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *buVk;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TButton *TMenuBlinchiki;
	TImage *Image1;
	TToolBar *ToolBar2;
	TListView *ListView1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TButton *buBackMenu;
	TLabel *laBlinchiki;
	TButton *Button7;
	TTabItem *TBlin;
	TToolBar *ToolBar3;
	TLabel *Blin;
	TButton *buBackBlinciki;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout2;
	TImage *Image2;
	TLabel *���;
	TLabel *Label3;
	TLabel *������������;
	TLabel *Label5;
	TLabel *����;
	TLabel *Label7;
	TMemo *Memo1;
	TGridPanelLayout *GridPanelLayout3;
	TButton *buBlinVKrozinu;
	TButton *buBistrZakaz;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkControlToField *LinkControlToField1;
	TGridPanelLayout *GridPanelLayout4;
	TButton *buMinus;
	TLabel *laNum;
	TButton *buPlus;
	TTabItem *TVk;
	TTabItem *TOrder;
	TToolBar *ToolBar4;
	TLabel *Label2;
	TButton *buBackBlin;
	TToolBar *ToolBar5;
	TLabel *Label4;
	TButton *buBackVk;
	TTabItem *TOtzov;
	TTabItem *TAddOtzov;
	TToolBar *ToolBar6;
	TLabel *������;
	TButton *Button2;
	TButton *addOtziv;
	TToolBar *ToolBar7;
	TLabel *Label6;
	TButton *buBackOtziv;
	TButton *buAddO;
	TListView *ListView2;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TGridPanelLayout *GridPanelLayout5;
	TLabel *Label8;
	TLabel *Label9;
	TEdit *edName;
	TLabel *Label10;
	TEdit *edPhone;
	TLabel *Label11;
	TEdit *edMail;
	TLabel *Label12;
	TEdit *edMessage;
	TButton *buOrder;
	TGridPanelLayout *GridPanelLayout6;
	TLabel *Label14;
	TEdit *edOrderPhone;
	TLabel *Label15;
	TLabel *Label16;
	TEdit *edOrderName;
	TEdit *edOrderMail;
	TLabel *Label13;
	TEdit *edOrderAdress;
	TLabel *Label17;
	TEdit *edOrderData;
	TLabel *Label18;
	TEdit *edOrderTime;
	TLabel *Label19;
	TEdit *edOrderPrice;
	TLabel *Label20;
	TEdit *edOrderPr;
	TGridLayout *glBucket;
	TButton *Button8;
	TTabItem *TDelivery;
	TToolBar *ToolBar8;
	TLabel *Label21;
	TButton *Button9;
	TLabel *Label22;
	TTabItem *TContackt;
	TToolBar *ToolBar9;
	TLabel *Label23;
	TButton *Button10;
	TLabel *Label24;
	TLabel *Label25;
	TLabel *Label26;
	void __fastcall TMenuBlinchikiClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBackMenuClick(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buBackBlincikiClick(TObject *Sender);
	void __fastcall buBlinVKrozinuClick(TObject *Sender);
	void __fastcall buMinusClick(TObject *Sender);
	void __fastcall buPlusClick(TObject *Sender);
	void __fastcall buBackBlinClick(TObject *Sender);
	void __fastcall buBackVkClick(TObject *Sender);
	void __fastcall buVkClick(TObject *Sender);
	void __fastcall buBistrZakazClick(TObject *Sender);
	void __fastcall addOtzivClick(TObject *Sender);
	void __fastcall buBackOtzivClick(TObject *Sender);
	void __fastcall buAddOClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall buOrderClick(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);

private:	// User declarations
//    struct product;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);

};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

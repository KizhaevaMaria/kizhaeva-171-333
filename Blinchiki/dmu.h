//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDConnection *FDConnection1;
	TFDQuery *quProduct;
	TIntegerField *quProductID;
	TIntegerField *quProductCATEGORY_ID;
	TWideStringField *quProductNAME;
	TBlobField *quProductIMAGE;
	TIntegerField *quProductGRAM;
	TIntegerField *quProductKCAL;
	TFloatField *quProductPRICE;
	TWideStringField *quProductNOTE;
	TIntegerField *quProductSTICKER;
	TFDStoredProc *spOrdering_ListINS;
	TFDQuery *quOrders;
	TIntegerField *quOrdersID;
	TSQLTimeStampField *quOrdersCREATE_DATETIME;
	TWideStringField *quOrdersCLIENT_FIO;
	TWideStringField *quOrdersCLIENT_TEL;
	TWideStringField *quOrdersCLIENT_EMAIL;
	TWideStringField *quOrdersCLIENT_ADDRESS;
	TDateField *quOrdersDELIVERY_DATE;
	TTimeField *quOrdersDELIVERY_TIME;
	TFloatField *quOrdersDELIVERY_AMOUNT;
	TWideStringField *quOrdersDELIVERY_NOTE;
	TIntegerField *quOrdersSTATUS;
	TWideStringField *quOrdersSTATUS_NOTE;
	TFDQuery *quOrderingList;
	TIntegerField *quOrderingListID;
	TIntegerField *quOrderingListORDER_ID;
	TIntegerField *quOrderingListPRODUCT_ID;
	TFloatField *quOrderingListPRODUCT_PRICE;
	TIntegerField *quOrderingListQUANTITY;
	TFDQuery *quReview;
	TWideStringField *quReviewNAME;
	TStringField *quReviewPHONE;
	TWideStringField *quReviewMAIL;
	TWideStringField *quReviewMESSAGE;
	TFDStoredProc *spReviewINS;
	TFDStoredProc *spOrderINS;
private:	// User declarations

public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
		void OrderListINS(int order_id, int product_id, double product_price,
		int quantity);
		void ReviewINS (UnicodeString name, UnicodeString phone,
		UnicodeString mail, UnicodeString mess);

		void OrderingINS(UnicodeString fio, UnicodeString tel,
		UnicodeString email, UnicodeString address, UnicodeString date,
		UnicodeString time, UnicodeString amount, UnicodeString note);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif

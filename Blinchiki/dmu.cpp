//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void Tdm::OrderListINS(int order_id, int product_id, double product_price,
	int quantity)  {
//		spOrderListINS->ParamByName("ORDER_ID")->Value = order_id;
//		spOrderListINS->ParamByName("PRODUCT_ID")->Value = product_id;
//		spOrderListINS->ParamByName("PRODUCT_PRICE")->Value = product_price;
//		spOrderListINS->ParamByName("QUANTITY")->Value = quantity;
	}
void Tdm::ReviewINS(UnicodeString name, UnicodeString phone,
		UnicodeString mail, UnicodeString mess) {
	spReviewINS->ParamByName("NAME")-> Value = name;
	spReviewINS->ParamByName("PHONE")-> Value = phone;
	spReviewINS->ParamByName("MAIL")-> Value = mail;
	spReviewINS->ParamByName("MESS")-> Value = mess;

	spReviewINS->ExecProc();
}
void Tdm::OrderingINS(UnicodeString fio, UnicodeString tel, UnicodeString email,
UnicodeString address, UnicodeString date, UnicodeString time,
UnicodeString amount, UnicodeString note){
	spOrderINS->ParamByName("CLIENT_FIO")-> Value = fio;
	spOrderINS->ParamByName("CLIENT_TEL")-> Value = tel;
	spOrderINS->ParamByName("CLIENT_EMAIL")-> Value = email;
	spOrderINS->ParamByName("CLIENT_ADDRESS")-> Value = address;
	spOrderINS->ParamByName("DELIVERY_DATE")-> Value = StrToDate(date);
	spOrderINS->ParamByName("DELIVERY_TIME")-> Value = StrToTime(time);
	spOrderINS->ParamByName("DELIVERY_AMOUNT")-> Value = (double)StrToInt(amount);
	spOrderINS->ParamByName("DELIVERY_NOTE")-> Value = note;

	spOrderINS->ExecProc();
}

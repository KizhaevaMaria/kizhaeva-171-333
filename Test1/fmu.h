//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TTest : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TMenu;
	TTabItem *TRandomNum;
	TTabItem *TSumNum;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buRandom;
	TButton *buSum;
	TLabel *laNum;
	TButton *buRand;
	TEdit *edNum;
	TButton *buCheck;
	TMemo *memo;
	TEdit *ed1;
	TEdit *ed2;
	TButton *buSumm;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buRandomClick(TObject *Sender);
	void __fastcall buSumClick(TObject *Sender);
	void __fastcall buRandClick(TObject *Sender);
	void __fastcall buCheckClick(TObject *Sender);
	void __fastcall buSummClick(TObject *Sender);
private:	// User declarations
	void Summ (String a, String b);
public:		// User declarations
	__fastcall TTest(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TTest *Test;
//---------------------------------------------------------------------------
#endif

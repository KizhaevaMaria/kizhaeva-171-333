//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TTest *Test;
//---------------------------------------------------------------------------
__fastcall TTest::TTest(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TTest::FormCreate(TObject *Sender)
{
	tc->GotoVisibleTab(TMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TTest::buRandomClick(TObject *Sender)
{
	tc->GotoVisibleTab(TRandomNum->Index);
}
//---------------------------------------------------------------------------
void __fastcall TTest::buSumClick(TObject *Sender)
{
	tc->GotoVisibleTab(TSumNum->Index);
}
//---------------------------------------------------------------------------
void __fastcall TTest::buRandClick(TObject *Sender)
{
	laNum->Text = Random(10);
}
//---------------------------------------------------------------------------
void __fastcall TTest::buCheckClick(TObject *Sender)
{
	laNum->Text = Random(10);
	if (edNum -> Text == laNum -> Text) {
		ShowMessage("Правильно");
	}
}
void __fastcall TTest::Summ(String a, String b){
	int Result = StrToInt(a) + StrToInt(b);
	memo->Lines->Add(IntToStr(Result));
}

//---------------------------------------------------------------------------
void __fastcall TTest::buSummClick(TObject *Sender)
{
	Summ(ed1->Text, ed2->Text);
}
//---------------------------------------------------------------------------


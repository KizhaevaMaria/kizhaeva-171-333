//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Colors.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buNewImage;
	TButton *buNewRect;
	TButton *buClear;
	TButton *buAbout;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TLayout *ly;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TSelection *Selection3;
	TRectangle *Rectangle1;
	TToolBar *tbOptions;
	TButton *buBringToFront;
	TButton *buSendToBack;
	TButton *buDel;
	TTrackBar *trRotation;
	TToolBar *tbImage;
	TButton *buImagePrev;
	TButton *buImageNext;
	TButton *buImageSelect;
	TButton *buImageRND;
	TToolBar *tbRect;
	TComboColorBox *ComboColorBoxRect;
	TTrackBar *trRectRadius;
	TSelection *Selection4;
	TText *Text1;
	TToolBar *tbText;
	TEdit *edText;
	TTrackBar *trText;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageSelectClick(TObject *Sender);
	void __fastcall buImageRNDClick(TObject *Sender);
	void __fastcall ComboColorBoxRectChange(TObject *Sender);
	void __fastcall trRectRadiusChange(TObject *Sender);
	void __fastcall buNewImageClick(TObject *Sender);
	void __fastcall buNewRectClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall trRotationChange(TObject *Sender);
	void __fastcall trTextChange(TObject *Sender);
	void __fastcall edTextChange(TObject *Sender);
private:	// User declarations
	TSelection *FSel;
    void SelectionAll(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

object dm: Tdm
  OldCreateOrder = False
  Height = 503
  Width = 583
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\'#1052#1072#1088#1080#1103' '#1050#1080#1078#1072#1077#1074#1072'\Documents\new folder with projec' +
        't\kizhaeva-171-333\BD\db\BD.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 80
    Top = 48
  end
  object quProduct: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    products.name,'
      '    products.number,'
      '    products.price'
      'from products'
      'order by products.name')
    Left = 176
    Top = 48
    object quProductNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 30
    end
    object quProductNUMBER: TIntegerField
      FieldName = 'NUMBER'
      Origin = 'NUMBER'
    end
    object quProductPRICE: TIntegerField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
  end
  object spProdUPD: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'PROD_UPD'
    Left = 80
    Top = 136
    ParamData = <
      item
        Position = 1
        Name = 'NUMBER'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 30
      end>
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 80
    Top = 232
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 200
    Top = 232
  end
  object spProdINS: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'NEW_PROCEDURE'
    Left = 216
    Top = 160
    ParamData = <
      item
        Position = 1
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 30
      end
      item
        Position = 2
        Name = 'NUMBER'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'PRICE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object spProdDel: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'PROD_DELETE'
    Left = 344
    Top = 136
    ParamData = <
      item
        Position = 1
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 30
      end>
  end
end

//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(TProduct->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab(TProduct->Index);
	dm->ProdUPD(laName->Text, StrToInt(laNum->Text));
	dm->quProduct->Refresh();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(TProd->Index);
	dm->quProduct->Refresh();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::changeNum(TObject *Sender)
{
	if(((TControl*)Sender)->Tag == 0){
		laNum->Text = IntToStr(StrToInt(laNum->Text)-1);
	}
	else{
		laNum->Text = IntToStr(StrToInt(laNum->Text)+1);
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	tc->GotoVisibleTab(TProduct->Index);
	dm->quProduct->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAddClick(TObject *Sender)
{
	if(edName->Text != "" || edNum->Text != "" || edPrice->Text != ""){
	dm->ProdINS(edName->Text, StrToInt(edNum->Text), StrToInt(edPrice->Text));
	dm->quProduct->Refresh();
	edName->Text = "";
	edNum->Text = "";
	edPrice->Text = "";
	ShowMessage("����� ������� ��������");
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
    tc->GotoVisibleTab(TAddProd->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buDelClick(TObject *Sender)
{
	dm->ProdDel(laName->Text);
	dm->quProduct->Refresh();
    tc->GotoVisibleTab(TProduct->Index);
	ShowMessage("������� ������");
}
//---------------------------------------------------------------------------


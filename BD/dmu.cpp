//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void Tdm::ProdUPD(UnicodeString aName, int aNumber){
	spProdUPD->ParamByName("NAME")-> Value = aName;
	spProdUPD->ParamByName("NUMBER")-> Value = aNumber;

	spProdUPD->ExecProc();
}

void Tdm::ProdINS(UnicodeString iName, int iNumber, int iPrice) {
	spProdINS->ParamByName("NAME")-> Value = iName;
	spProdINS->ParamByName("NUMBER")-> Value = iNumber;
    spProdINS->ParamByName("PRICE")-> Value = iPrice;

	spProdINS->ExecProc();
}

void Tdm::ProdDel(UnicodeString dName){
	spProdDel->ParamByName("NAME")-> Value = dName;

	spProdDel->ExecProc();
}

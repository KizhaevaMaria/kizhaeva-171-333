//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TListView *ListView1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TTabControl *tc;
	TTabItem *TProduct;
	TTabItem *TProd;
	TToolBar *ToolBar1;
	TButton *Button1;
	TLabel *Label1;
	TLabel *Label3;
	TLabel *Label5;
	TLabel *laName;
	TLabel *laPrice;
	TLabel *laNum;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLabel *Label2;
	TButton *Button2;
	TButton *Button3;
	TTabItem *TAddProd;
	TLabel *Label4;
	TLabel *Label7;
	TLabel *Label6;
	TToolBar *ToolBar2;
	TButton *Button4;
	TLabel *Label8;
	TEdit *edName;
	TEdit *edPrice;
	TEdit *edNum;
	TButton *buAdd;
	TButton *Button5;
	TButton *buDel;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall changeNum(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

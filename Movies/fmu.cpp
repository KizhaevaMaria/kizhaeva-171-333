//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(TMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMoviesClick(TObject *Sender)
{
	tc->GotoVisibleTab(TMovies->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buGenreClick(TObject *Sender)
{
		tc->GotoVisibleTab(TGenre->Index);
}
//---------------------------------------------------------------------------
//void __fastcall Tfm::buBackMenu(TObject *Sender)
//{
//	tc->GotoVisibleTab(TMenu->Index);
//}
//---------------------------------------------------------------------------
void __fastcall Tfm::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
		tc->GotoVisibleTab(TMovie->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView2ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
		dm->quMovG->Close();
		dm->quMovG->ParamByName("GENRE.ID")->Value = IntToStr(dm->quGenreID->Value);
		dm->quMovG->Open();
		tc->GotoVisibleTab(TMovGenre->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackGClick(TObject *Sender)
{
		tc->GotoVisibleTab(TGenre->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab(TMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackMenuClick(TObject *Sender)
{
    tc->GotoVisibleTab(TMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->GotoVisibleTab(TMovies->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView3ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
		tc->GotoVisibleTab(TMovieGenre->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{

		tc->GotoVisibleTab(TMovGenre->Index);
}
//---------------------------------------------------------------------------


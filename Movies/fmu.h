//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TMenu;
	TTabItem *TMovies;
	TTabItem *TGenre;
	TTabItem *TMovie;
	TButton *buMovies;
	TButton *buGenre;
	TListView *ListView1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TListView *ListView2;
	TToolBar *ToolBar1;
	TButton *buBackMenu;
	TLabel *Label1;
	TToolBar *ToolBar2;
	TButton *Button1;
	TLabel *Label2;
	TTabItem *TMovGenre;
	TToolBar *ToolBar3;
	TLabel *laMovie;
	TButton *Button2;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLabel *laDesc;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TListView *ListView3;
	TBindSourceDB *BindSourceDB3;
	TLinkListControlToField *LinkListControlToField3;
	TToolBar *ToolBar4;
	TLabel *Label3;
	TButton *buBackG;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TTabItem *TMovieGenre;
	TToolBar *ToolBar5;
	TLabel *Label4;
	TButton *Button3;
	TLabel *Label5;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buMoviesClick(TObject *Sender);
	void __fastcall buGenreClick(TObject *Sender);
//   	void __fastcall buBackGenre(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall ListView2ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buBackGClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buBackMenuClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall ListView3ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button3Click(TObject *Sender);



private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

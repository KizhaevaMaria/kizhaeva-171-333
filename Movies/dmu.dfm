object dm: Tdm
  OldCreateOrder = False
  Height = 530
  Width = 631
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\'#1052#1072#1088#1080#1103' '#1050#1080#1078#1072#1077#1074#1072'\Documents\new folder with projec' +
        't\kizhaeva-171-333\Movies\db\MOVIES.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 32
    Top = 24
  end
  object quMovies: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    movies.title,'
      '    movies.description,'
      '    movies.id,'
      '    movies.genre_id'
      'from movies'
      'order by movies.title')
    Left = 144
    Top = 24
    object quMoviesTITLE: TWideStringField
      FieldName = 'TITLE'
      Origin = 'TITLE'
      FixedChar = True
      Size = 100
    end
    object quMoviesDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
      FixedChar = True
      Size = 300
    end
    object quMoviesID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quMoviesGENRE_ID: TIntegerField
      FieldName = 'GENRE_ID'
      Origin = 'GENRE_ID'
    end
  end
  object quGenre: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    genre.name,'
      '    genre.id'
      'from genre'
      'order by genre.name')
    Left = 208
    Top = 24
    ParamData = <
      item
        Position = 1
        ParamType = ptInput
      end>
    object quGenreNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 100
    end
    object quGenreID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 48
    Top = 240
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 152
    Top = 232
  end
  object FDStoredProc1: TFDStoredProc
    Connection = FDConnection1
    Left = 312
    Top = 112
  end
  object quMovG: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    movies.title,'
      '    movies.description,'
      '    movies.id,'
      '    movies.genre_id'
      'from movies'
      'where movies.genre_id=:genre.id')
    Left = 280
    Top = 40
    ParamData = <
      item
        Name = 'GENRE.ID'
        DataType = ftString
        ParamType = ptInput
        Value = '3'
      end>
    object quMovGTITLE: TWideStringField
      FieldName = 'TITLE'
      Origin = 'TITLE'
      FixedChar = True
      Size = 100
    end
    object quMovGDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
      FixedChar = True
      Size = 300
    end
    object quMovGID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quMovGGENRE_ID: TIntegerField
      FieldName = 'GENRE_ID'
      Origin = 'GENRE_ID'
    end
  end
end

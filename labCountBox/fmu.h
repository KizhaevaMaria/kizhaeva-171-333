//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *tiMenu;
	TTabItem *tiOptions;
	TTabItem *tiPlay;
	TTabItem *TabItem6;
	TLayout *lyTop;
	TButton *buReset;
	TLabel *laQuestion;
	TLabel *laTime;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer6;
	TButton *buAnswer5;
	TLayout *Layout2;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle18;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TRectangle *Rectangle10;
	TTimer *tmPlay;
	TRectangle *Rectangle13;
	TRectangle *Rectangle17;
	TRectangle *Rectangle19;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	//void __fastcall GridPanelLayout2Click(TObject *Sender);
	void __fastcall buAnswerAllClick(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	//void __fastcall DoReset(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimeValue;
	TList *FListBox;
	TList *FListAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer (int aValue);
	void DoFinish();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//-------
	const int cMaxBox = 25;
	const int cMaxAnswer = 6;
	const int cMinPossible = 4;
	const int cMaxPossible = 14;
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

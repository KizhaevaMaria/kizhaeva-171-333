//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop
#include "uUtils.h"
#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
passwordEdit->Text = RandomStr(StrToIntDef(pasLength->Text, 9),
lowerCaseCheck->IsChecked, upperCaseCheck->IsChecked,
numberCheck->IsChecked, specialCheck->IsChecked);
}
//---------------------------------------------------------------------------

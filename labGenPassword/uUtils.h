//---------------------------------------------------------------------------

#ifndef uUtilsH
#define uUtilsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

UnicodeString RandomStr(int aLength, bool aLower, bool aUpper,
bool aNumber, bool aSpec);
#endif

object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 284
  Width = 338
  object FDConnection1: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      
        'Database=C:\Users\'#1052#1072#1088#1080#1103' '#1050#1080#1078#1072#1077#1074#1072'\Documents\new folder with projec' +
        't\kizhaeva-171-333\CookBook\db\COOKBOOK.FDB'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    BeforeConnect = FDConnection1BeforeConnect
    Left = 48
    Top = 96
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 176
    Top = 96
  end
  object quRecipe: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    recipe.id,'
      '    recipe.favorite,'
      '    recipe.name,'
      '    recipe.ingredients,'
      '    recipe.text,'
      '    recipe.category'
      'from recipe'
      'order by recipe.name')
    Left = 176
    Top = 40
    object quRecipeID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quRecipeFAVORITE: TBooleanField
      FieldName = 'FAVORITE'
      Origin = 'FAVORITE'
    end
    object quRecipeNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 1000
    end
    object quRecipeINGREDIENTS: TWideStringField
      FieldName = 'INGREDIENTS'
      Origin = 'INGREDIENTS'
      FixedChar = True
      Size = 1000
    end
    object quRecipeTEXT: TWideStringField
      FieldName = 'TEXT'
      Origin = 'TEXT'
      FixedChar = True
      Size = 4000
    end
    object quRecipeCATEGORY: TWideStringField
      FieldName = 'CATEGORY'
      Origin = 'CATEGORY'
      FixedChar = True
      Size = 1000
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 72
    Top = 32
  end
  object spRecipeIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'INS_RECIPE'
    Left = 88
    Top = 168
    ParamData = <
      item
        Position = 1
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 100
      end
      item
        Position = 2
        Name = 'INGREDIENTS'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 1000
      end
      item
        Position = 3
        Name = 'TEXT'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 3000
      end
      item
        Position = 4
        Name = 'CATEGORY'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 100
      end>
  end
  object spFavorite: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'UPD_FAVORITE'
    Left = 176
    Top = 168
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftSmallint
        ParamType = ptInput
      end>
  end
  object quFavorite: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    recipe.id,'
      '    recipe.name,'
      '    recipe.ingredients,'
      '    recipe.text,'
      '    recipe.category,'
      '    recipe.favorite'
      'from recipe'
      'where '
      '   ('
      '      (favorite = true)'
      '   )')
    Left = 240
    Top = 152
    object quFavoriteID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quFavoriteNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 1000
    end
    object quFavoriteINGREDIENTS: TWideStringField
      FieldName = 'INGREDIENTS'
      Origin = 'INGREDIENTS'
      FixedChar = True
      Size = 1000
    end
    object quFavoriteTEXT: TWideStringField
      FieldName = 'TEXT'
      Origin = 'TEXT'
      FixedChar = True
      Size = 4000
    end
    object quFavoriteCATEGORY: TWideStringField
      FieldName = 'CATEGORY'
      Origin = 'CATEGORY'
      FixedChar = True
      Size = 1000
    end
    object quFavoriteFAVORITE: TBooleanField
      FieldName = 'FAVORITE'
      Origin = 'FAVORITE'
    end
  end
end

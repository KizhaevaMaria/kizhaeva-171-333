// ---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TTabControl *tc;
	TTabItem *tiRecipeList;
	TTabItem *tiRecipe;
	TTabItem *tiAddRecipe;
	TTabItem *tiMenu;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buRecipeList;
	TButton *buAddRecipeTi;
	TListView *ListView1;
	TScrollBox *ScrollBox1;
	TLayout *Layout1;
	TButton *Button1;
	TLabel *Label1;
	TLayout *Layout2;
	TButton *buBack;
	TLabel *lbName;
	TText *textIngredients;
	TMemo *meRecipeText;
	TLabel *Label3;
	TLabel *Label4;
	TText *textCategory;
	TLayout *Layout3;
	TButton *Button3;
	TLabel *Label5;
	TLabel *Label6;
	TEdit *edCategory;
	TEdit *edIngredients;
	TLabel *Label7;
	TEdit *edName;
	TLabel *Label8;
	TLabel *Label9;
	TMemo *meText;
	TLabel *Label10;
	TLayout *Layout4;
	TLayout *Layout5;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkControlToField *LinkControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TButton *buAddRecipe;
	TLinkListControlToField *LinkListControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TButton *buChangeFavorite;
	TButton *buFavorite;
	TTabItem *tiFavorite;
	TListView *ListView2;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TToolBar *ToolBar1;
	TButton *Button2;
	TStyleBook *StyleBook1;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buRecipeListClick(TObject *Sender);
	void __fastcall buAddRecipeTiClick(TObject *Sender);
	void __fastcall allBuBack(TObject *Sender);
	void __fastcall tiRecipeClick(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender,
		TListViewItem * const AItem);
	void __fastcall buAddRecipeClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buChangeFavoriteClick(TObject *Sender);
	void __fastcall buFavoriteClick(TObject *Sender);
	void __fastcall ListView2ItemClick(TObject * const Sender,
		TListViewItem * const AItem);

private: // User declarations
		boolean fromFavorites = false;

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif

// ---------------------------------------------------------------------------

#pragma hdrstop

#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;

// ---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner) : TDataModule(Owner) {
}

// ---------------------------------------------------------------------------
void Tdm::RecipeIns(UnicodeString aName, UnicodeString aIngredients,
	UnicodeString aText, UnicodeString aCategory) {
	spRecipeIns->ParamByName("NAME")->Value = aName;
	spRecipeIns->ParamByName("INGREDIENTS")->Value = aIngredients;
	spRecipeIns->ParamByName("TEXT")->Value = aText;
	spRecipeIns->ParamByName("CATEGORY")->Value = aCategory;
	spRecipeIns->ExecProc();
}

void Tdm::RecipeUpdFavorite(int id) {
	spFavorite->ParamByName("ID")->Value = id;
	spFavorite->ExecProc();
}
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
    FDConnection1 -> Connected = True;
}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnection1BeforeConnect(TObject *Sender)
{
	FDConnection1->Params->Values["Database"] = "..\\..\\db\\COOKBOOK.FDB";
}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnection1AfterConnect(TObject *Sender)
{
	quRecipe->Open();
    quFavorite->Open();
}
//---------------------------------------------------------------------------


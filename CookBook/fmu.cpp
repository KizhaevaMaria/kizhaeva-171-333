// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buRecipeListClick(TObject *Sender) {
	tc->GotoVisibleTab(tiRecipeList->Index);
	dm->quFavorite-> Refresh ();
	dm->quRecipe-> Refresh ();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buAddRecipeTiClick(TObject *Sender) {
	tc->GotoVisibleTab(tiAddRecipe->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::allBuBack(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tiRecipeClick(TObject *Sender) {
	tc->GotoVisibleTab(tiRecipeList->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::ListView1ItemClick(TObject * const Sender,
	TListViewItem * const AItem)

{
	fromFavorites = false;
	if (dm->quRecipeFAVORITE->Value) {
		buChangeFavorite->Text = L"������ �� ����������";
	}
	else {
		buChangeFavorite->Text = L"�������� � ���������";
	}
	tc->GotoVisibleTab(tiRecipe->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buAddRecipeClick(TObject *Sender) {
	dm->RecipeIns(edName->Text, edIngredients->Text, meText->Text,
		edCategory->Text);
	ShowMessage("������ ��������");
	tc->GotoVisibleTab(tiMenu->Index);
	//RecipeList
	dm->quRecipe-> Refresh ();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buBackClick(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buChangeFavoriteClick(TObject *Sender) {

	if (buChangeFavorite->Text == L"������ �� ����������")
	{
		buChangeFavorite->Text = L"�������� � ���������";
	}
	else {
		buChangeFavorite->Text = L"������ �� ����������";
	}
	if (fromFavorites) {
		dm->RecipeUpdFavorite(dm->quFavoriteID->Value);
	}
	else {
		dm->RecipeUpdFavorite(dm->quRecipeID->Value);
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buFavoriteClick(TObject *Sender) {
	dm->quFavorite-> Refresh ();
	dm->quRecipe-> Refresh ();
	tc->GotoVisibleTab(tiFavorite->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::ListView2ItemClick(TObject * const Sender,
	TListViewItem * const AItem)

{
	fromFavorites = true;
	if (dm->quFavoriteFAVORITE->Value) {
		buChangeFavorite->Text = L"������ �� ����������";
	}
	else {
		buChangeFavorite->Text = L"�������� � ���������";
	}
	lbName->Text = dm->quFavoriteNAME->Value;
	textCategory->Text = dm->quFavoriteCATEGORY->Value;
	textIngredients->Text = dm->quFavoriteINGREDIENTS->Value;
	meRecipeText->Text = dm->quFavoriteTEXT->Value;
	tc->GotoVisibleTab(tiRecipe->Index);
}
// ---------------------------------------------------------------------------


//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDQuery *quRecipe;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDStoredProc *spRecipeIns;
	TFDStoredProc *spFavorite;
	TFDQuery *quFavorite;
	TIntegerField *quRecipeID;
	TBooleanField *quRecipeFAVORITE;
	TWideStringField *quRecipeNAME;
	TWideStringField *quRecipeINGREDIENTS;
	TWideStringField *quRecipeTEXT;
	TWideStringField *quRecipeCATEGORY;
	TIntegerField *quFavoriteID;
	TWideStringField *quFavoriteNAME;
	TWideStringField *quFavoriteINGREDIENTS;
	TWideStringField *quFavoriteTEXT;
	TWideStringField *quFavoriteCATEGORY;
	TBooleanField *quFavoriteFAVORITE;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall FDConnection1BeforeConnect(TObject *Sender);
	void __fastcall FDConnection1AfterConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
void RecipeIns(UnicodeString aName, UnicodeString aIngredients,
		UnicodeString aText, UnicodeString aCategory);
void RecipeUpdFavorite(int id);
	__fastcall Tdm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif

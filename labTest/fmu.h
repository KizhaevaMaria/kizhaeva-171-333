//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TButton *buInfo;
	TLabel *laQuestion;
	TTabControl *tabCtrl;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TButton *Start;
	TImage *Image1;
	TScrollBox *ScrollBox1;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TImage *Image2;
	TScrollBox *ScrollBox2;
	TLabel *Label3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TImage *Image3;
	TScrollBox *ScrollBox3;
	TLabel *Label4;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TImage *Image4;
	TButton *buRestart;
	TMemo *memo;
	TProgressBar *pb;
	TLabel *laCorrect;
	TLabel *laWrong;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall StartClick(TObject *Sender);
	void __fastcall ButtonClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall tabCtrlChange(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

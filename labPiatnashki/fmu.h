//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TGridPanelLayout *GridPanelLayout1;
	TImageList *ImageList;
	TGlyph *Glyph1;
	TGlyph *Glyph2;
	TGlyph *Glyph3;
	TGlyph *Glyph4;
	TGlyph *Glyph5;
	TGlyph *Glyph6;
	TGlyph *Glyph7;
	TGlyph *Glyph8;
	TGlyph *Glyph9;
	TGlyph *Glyph10;
	TGlyph *Glyph11;
	TGlyph *Glyph12;
	TGlyph *Glyph13;
	TGlyph *Glyph14;
	TGlyph *Glyph15;
	TGlyph *Glyph16;
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
    TGlyph *m[16];
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
